import { Request } from 'express';

export interface IAuth {
  username: string;
  password: string;
}

export interface IUserPatch {
  oldPassword: string;
  newPassword: string;
}

export interface IUserAuth {
  id: string;
  username: string;
}

export interface AuthRequest extends Request {
  user: IUserAuth
}

export interface INote {
  text: string;
}
