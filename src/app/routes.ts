import { json, urlencoded } from 'express';
import { Express } from 'express-serve-static-core';
import authRouter from './controllers/auth.controller';
import userRouter from './controllers/user.controllers';
import noteRouter from './controllers/note.controller';
import errorHandler from './middleware/error-handler';

function initRoutes(app: Express) {
  app.use(json());
  app.use(urlencoded({ extended: false }));
  app.use('/api/auth', authRouter);
  app.use('/api/users/me', userRouter);
  app.use('/api/notes', noteRouter);
  app.use(errorHandler);
}

export default initRoutes;
