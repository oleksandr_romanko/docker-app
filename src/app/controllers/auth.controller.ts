import { genSalt, hash, compare } from 'bcryptjs';
import { Request, Response, Router } from 'express';
import asyncHandler from '../middleware/async-handler';
import { UserModel } from '../models/User';
import authValidators from '../validation/auth.validators';

const authRouter = Router();

authRouter.post('/register', asyncHandler(async (req: Request, res: Response) => {
  const { error } = authValidators(req.body);

  if (error) {
    res.status(400).json({ message: error.details[0].message });
    return;
  }

  let user = await UserModel.findOne({ username: req.body.username });

  if (user) {
    res.status(400).json({ message: 'User has already exists' });
    return;
  }
  const { username, password } = req.body;

  user = new UserModel({ username, password });

  const salt = await genSalt(10);
  user.password = await hash(user.password, salt);

  await user.save();

  const token = user.generateAuthToken();

  res.header('x-auth-token', token).json({ message: 'User has been registered succesfully' });
}));

authRouter.post('/login', async (req: Request, res: Response) => {
  const { error } = await authValidators(req.body);

  if (error) {
    res.status(400).json({ message: error.details[0].message });
    return;
  }

  const user = await UserModel.findOne({ username: req.body.username });

  if (!user) {
    res.status(400).json({ message: 'User has no found' });
    return;
  }

  const validPassword = await compare(req.body.password, user.password);

  if (!validPassword) {
    res.status(400).send({ error: 'Invalid password' });
    return;
  }

  const token = user.generateAuthToken();

  res.json({ message: 'Succses', auth_token: token });
});

export default authRouter;
