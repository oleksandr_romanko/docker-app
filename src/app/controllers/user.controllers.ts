import { compare, genSalt, hash } from 'bcryptjs';
import { Response, Router } from 'express';
import userPatchValidators from '../validation/userPatch.validators';
import { AuthRequest, IUserPatch } from '../interfaces';
import { UserModel } from '../models/User';
import auth from '../middleware/auth';
import asyncHandler from '../middleware/async-handler';

const userRouter = Router();

userRouter.post('/', auth, asyncHandler(async (req: AuthRequest, res: Response) => {
  const user = await UserModel.findById(req.user.id).select('-password');

  if (!user) {
    res.status(400).json({ message: 'User has no found' });
    return;
  }

  res.json({ user });
}));

userRouter.delete('/', auth, asyncHandler(async (req: AuthRequest, res: Response) => {
  const user = await UserModel.findByIdAndRemove(req.user.id);

  if (!user) {
    res.status(400).json({ message: 'User has no found' });
    return;
  }

  res.json({ message: 'Succses' });
}));

userRouter.patch('/', auth, asyncHandler(async (req: AuthRequest, res: Response) => {
  const { error } = userPatchValidators(req.body);

  if (error) {
    res.status(400).json({ message: error.details[0].message });
    return;
  }

  const { oldPassword, newPassword }: IUserPatch = req.body;

  const user = await UserModel.findById(req.user.id);

  if (!user) {
    res.status(400).json({ message: 'User has no found' });
    return;
  }

  const isMatched = await compare(oldPassword, user.password);
  if (!isMatched) {
    res.status(400).json({ message: 'The old password has nomatch' });
    return;
  }

  const salt = await genSalt(10);
  const hashPassword = await hash(newPassword, salt);

  await UserModel.findByIdAndUpdate(req.user.id, { password: hashPassword }, { new: true });

  res.json({ message: 'Succses' });
}));

export default userRouter;
