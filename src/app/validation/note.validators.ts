import * as Joi from 'joi';
import { INote } from '../interfaces';

export default function authValidators(body: INote) {
  const schema = Joi.object({
    text: Joi.string().min(1).required(),
  });

  return schema.validate(body);
}
