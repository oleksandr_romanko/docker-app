# NodeJS App written in Typescript, using Express and MongoDB, deployed via Docker

Created this app to improve my JavaScript knowledge and transition from Frontend to Full-Stack JavaScript.

### Local machine setup
- install NodeJS on your local machine (https://nodejs.org/en/)
- install docker-compose on your machine (https://docs.docker.com/compose/install/)

### Local project setup
- clone or download this repo
- open terminal (powershell etc.) in the project folder
- run `npm install`
- compile the app (look in package.json) `npm run build`
- if you get any tsc error you might need to install typescript globally (`npm i -g typescript`)
- run `docker-compose up`, this will create 2 containers mongo (port 27017), mongo-express (port 3000)

### Project details
- POST:   `localhost:3000/api​/auth​/register` - register a new user
- POST:   `localhost:3000/api​/auth​/login`    - login
- GET:    `localhost:3000/api​/users​/me`      - get user's profile info
- DELETE: `localhost:3000/api​/users​/me`      - delete user's profile
- PATCH:  `localhost:3000/api​/users​/me`      - change user's password
- GET:    `localhost:3000/api​/notes`         - Get user's notes
- POST:   `localhost:3000/api​/notes`         - add Note for User
- GET:    `localhost:3000/api​/notes​/{id}`    - get user's note by id
- PUT:    `localhost:3000/api​/notes​/{id}`    - update user's note by id
- PATCH:  `localhost:3000/api​/notes​/{id}`    - check/uncheck user's note by id
- DELETE: `localhost:3000/api​/notes​/{id}`    - delete user's note by id
