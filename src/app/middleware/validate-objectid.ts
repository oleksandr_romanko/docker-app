import { NextFunction, Request, Response } from 'express';
import { Types } from 'mongoose';

export default function validateObjectId(req: Request, res: Response, next: NextFunction) {
  if (!Types.ObjectId.isValid(req.params.id)) {
    res.status(404).json('Invalid id');
  }

  next();
}
