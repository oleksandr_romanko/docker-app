import { sign } from 'jsonwebtoken';
import {
  Schema, model, Document, Model,
} from 'mongoose';

export interface IUser extends Document {
  username: string;
  password: string;
  createdDate: Date;
}

export interface IUserModel extends IUser {
  generateAuthToken(): string;
}

const UserSchema = new Schema<IUser>({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

UserSchema.methods.generateAuthToken = function () {
  return sign({ id: this._id, username: this.username }, process.env.JWT_SECRET!);
};

export const UserModel:Model<IUserModel> = model<IUserModel>('User', UserSchema);
