import * as Joi from 'joi';
import { IAuth } from '../interfaces';

export default function authValidators(auth: IAuth) {
  const schema = Joi.object({
    username: Joi.string().min(4).max(25).required(),
    password: Joi.string().required().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  return schema.validate(auth);
}
