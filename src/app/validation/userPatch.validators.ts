import * as Joi from 'joi';
import { IUserPatch } from '../interfaces';

export default function userPatchValidators(body: IUserPatch) {
  const schema = Joi.object({
    newPassword: Joi.string().required().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    oldPassword: Joi.string().required().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  return schema.validate(body);
}
