import * as express from 'express';
import * as morgan from 'morgan';
import * as path from 'path';
import * as rfs from 'rotating-file-stream';
import * as dotenv from 'dotenv';
import * as mongoose from 'mongoose';
import initRoutes from './app/routes';

const app = express();

const result = dotenv.config();
if (result.error) {
  dotenv.config({ path: '.env.default' });
}

app.use(express.static(path.resolve(__dirname, 'client')));

const accessLogStream = rfs.createStream('access.log', {
  interval: '1d',
  path: path.join(__dirname, '../log'),
});

app.use(morgan('common', { stream: accessLogStream }));

initRoutes(app);

const PORT = process.env.PORT || 3000;
const MONGO_URL = process.env.MONGO_URL!;

const start = async () => {
  try {
    await mongoose.connect(MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    app.listen(PORT, () => {
    });
  } catch (e) {
    process.exit(1);
  }
};

start();
