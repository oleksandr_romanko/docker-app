import { Request, Response } from 'express';
/**
 * Middleware which will be declared at the end of all routes,
 * this will globally handle any 500 errors which need
 * to be logged.
 * @param error
 * @param req
 * @param res
 */
export default function errorHandler(error: Error, req: Request, res: Response) {
  res.status(500).json({ message: 'Server error' });
}
