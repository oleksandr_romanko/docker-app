import { Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';
import { AuthRequest, IUserAuth } from '../interfaces';

export default function auth(req: AuthRequest, res: Response, next: NextFunction) {
  const token = req.header('x-auth-token')!;

  if (!token) {
    res.status(401).json('Access denied. No token provided.');
  }

  try {
    req.user = verify(token, process.env.JWT_SECRET!) as IUserAuth;
    next();
  } catch (error) {
    res.status(400).json('Invalid token');
  }
}
