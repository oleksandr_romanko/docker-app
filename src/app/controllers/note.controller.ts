import { Response, Router } from 'express';
import auth from '../middleware/auth';
import NoteModel from '../models/Note';
import validateObjectId from '../middleware/validate-objectid';
import noteValidators from '../validation/note.validators';
import { AuthRequest } from '../interfaces';
import asyncHandler from '../middleware/async-handler';

const noteRouter = Router();

noteRouter.get('/', auth, asyncHandler(async (req: AuthRequest, res: Response) => {
  const notes = await NoteModel.find({ username: req.user.username });

  if (!notes) {
    res.status(400).json({ message: 'Notes has no found' });
    return;
  }

  res.json({ notes });
}));

noteRouter.post('/', auth, asyncHandler(async (req: AuthRequest, res: Response) => {
  const { error } = noteValidators(req.body);

  if (error) {
    res.status(400).json({ message: error.details[0].message });
    return;
  }

  const note = new NoteModel({ text: req.body.text, userId: req.user.id });

  await note.save();

  res.json({ message: 'Succses' });
}));

noteRouter.get('/:id', auth, validateObjectId, asyncHandler(async (req: AuthRequest, res: Response) => {
  const note = await NoteModel.find({ _id: req.params.id, userId: req.user.id });

  if (!note) {
    res.status(400).json({ message: 'Note has no found' });
    return;
  }

  res.json({ note });
}));

noteRouter.put('/:id', auth, validateObjectId, asyncHandler(async (req: AuthRequest, res: Response) => {
  const { error } = await noteValidators(req.body);

  if (error) {
    res.status(400).json({ message: error.details[0].message });
    return;
  }

  const note = await NoteModel.findOneAndUpdate(
    { _id: req.params.id, userId: req.user.id },
    { text: req.body.text },
    { new: true },
  );

  if (!note) {
    res.status(400).json({ message: 'Note has no found' });
    return;
  }

  res.json({ message: 'Succses' });
}));

noteRouter.patch('/:id', auth, validateObjectId, asyncHandler(async (req: AuthRequest, res: Response) => {
  const note = await NoteModel.findOne({
    _id: req.params.id,
    userId: req.user.id,
  });

  let update;
  if (note && 'completed' in note) {
    update = await NoteModel.findOneAndUpdate(
      { _id: req.params.id, userId: req.user.id },
      { completed: !note.completed },
      { new: true },
    );
  }

  if (!update) {
    res.status(400).json({ message: 'Note has no found' });
    return;
  }

  res.json({ message: 'Succses' });
}));

noteRouter.delete('/:id', auth, validateObjectId, asyncHandler(async (req: AuthRequest, res: Response) => {
  const note = await NoteModel.findOneAndDelete({ _id: req.params.id, userId: req.user.id });

  if (!note) {
    res.status(400).json({ message: 'Note has no found' });
    return;
  }

  res.json({ message: 'Succses' });
}));

export default noteRouter;
